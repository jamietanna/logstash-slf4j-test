package me.jvt.hacking;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

public class ClassThatLogs {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassThatLogs.class);

    public void methodToLog() {
        LOGGER.info("Message", keyValue("object", this));
    }
}
