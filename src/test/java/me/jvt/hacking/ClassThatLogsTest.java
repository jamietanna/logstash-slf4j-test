package me.jvt.hacking;

import org.junit.jupiter.api.Test;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.org.lidalia.slf4jtest.LoggingEvent.info;

class ClassThatLogsTest {

    private final TestLogger logger = TestLoggerFactory.getTestLogger(ClassThatLogs.class);
    private final ClassThatLogs clazz = new ClassThatLogs();

    @Test
    void testLogs() {
        clazz.methodToLog();

        assertThat(logger.getLoggingEvents()).contains(info("Message"));
    }
}
